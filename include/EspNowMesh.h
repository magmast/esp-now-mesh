#ifndef ESP_NOW_MESH_H
#define ESP_NOW_MESH_H

#include "Magmast/EspNow/Mesh/Mesh.h"

using EspNowMeshPayload = Magmast::EspNow::Mesh::SendPacket::Payload;
using EspNowMeshNode = Magmast::EspNow::Mesh::Node;

extern Magmast::EspNow::Mesh::Mesh &EspNowMesh;

#endif