#ifndef MAGMAST_ESP_NOW_MESH_CODEC_H
#define MAGMAST_ESP_NOW_MESH_CODEC_H

#include <array>

#include <etl/array_view.h>
#include <etl/vector.h>

#include "Packet.h"

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            class Encoder
            {
            public:
                constexpr Encoder() = default;

                etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
                encode(const Packet &packet) const;

                etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
                encode(const PingPacket &packet) const;

                etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
                encode(const PongPacket &packet) const;

                etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
                encode(const SendPacket &packet) const;
            };

            class Decoder
            {
            public:
                constexpr Decoder() = default;

                Packet decode(const etl::array_view<uint8_t> &data) const;
                Packet decodePing(const etl::array_view<uint8_t> &data) const;
                Packet decodePong(const etl::array_view<uint8_t> &data) const;
                Packet decodeSend(const etl::array_view<uint8_t> &data) const;
            };

            class Codec
            {
            public:
                constexpr Codec() = default;

                etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
                encode(const Packet &packet) const;

                etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
                encode(const PingPacket &packet) const;

                etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
                encode(const PongPacket &packet) const;

                etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
                encode(const SendPacket &packet) const;

                Packet decode(const etl::array_view<uint8_t> &data) const;

            private:
                Encoder encoder;
                Decoder decoder;
            };
        }
    }
}

#endif