#ifndef MAGMAST_ESP_NOW_MESH_PACKET_H
#define MAGMAST_ESP_NOW_MESH_PACKET_H

#include <EspNow.h>
#include <etl/array_view.h>
#include <etl/delegate.h>
#include <etl/variant.h>

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            class PingPacket;

            class PongPacket;

            class SendPacket;

            /// Defines common packet properties.
            class IPacket
            {
            public:
                static constexpr const size_t MAX_SIZE_IN_BYTES = 250;

                using Address = EspNowAddress;

                virtual ~IPacket() = default;

                virtual const Address &getSender() const = 0;

                virtual bool isPing() const
                {
                    return false;
                }

                virtual bool isPong() const
                {
                    return false;
                }

                virtual bool isSend() const
                {
                    return false;
                }
            };

            class PingPacket : public IPacket
            {
            public:
                explicit PingPacket(const Address &&sender);

                const Address &getSender() const override;

                bool isPing() const override;

            private:
                Address _sender;
            };

            class PongPacket : public IPacket
            {
            public:
                explicit PongPacket(const Address &&sender);

                const Address &getSender() const override;

                bool isPong() const override;

            private:
                Address _sender;
            };

            class SendPacket : public IPacket
            {
            public:
                using Id = uint32_t;
                using Payload = etl::array_view<uint8_t>;

                SendPacket(
                    Id id,
                    const EspNowAddress &&sender,
                    const EspNowAddress &&receiver,
                    const etl::array_view<uint8_t> &&payload);

                Id getId() const;

                const Address &getSender() const override;
                bool isSentBy(const Address &other) const;

                const Address &getReceiver() const;
                bool isSentTo(const Address &other) const;
                bool isSentTo(const EspNowPeer &peer) const;

                const Payload &getPayload() const;

                bool isSend() const override;

            private:
                Id _id;
                Address _sender;
                Address _receiver;
                Payload _payload;
            };

            /// Any type of packet.
            class Packet : public IPacket
            {
            public:
                static Packet ping(const Address &&sender);

                static Packet pong(const Address &&sender);

                static Packet send(
                    SendPacket::Id id,
                    const EspNowAddress &&sender,
                    const EspNowAddress &&receiver,
                    const etl::array_view<uint8_t> &&payload);

                explicit Packet(const PingPacket &&packet);

                explicit Packet(const PongPacket &&packet);

                explicit Packet(const SendPacket &&packet);

                const Address &getSender() const override;

                bool isPing() const override;

                bool isPong() const override;

                bool isSend() const override;

                template <typename T>
                T map(
                    std::function<T(const PingPacket &packet)> ping,
                    std::function<T(const PongPacket &packet)> pong,
                    std::function<T(const SendPacket &packet)> send)
                    const
                {
                    if (etl::holds_alternative<PingPacket>(_packet))
                    {
                        return ping(etl::get<PingPacket>(_packet));
                    }
                    if (etl::holds_alternative<PongPacket>(_packet))
                    {
                        return pong(etl::get<PongPacket>(_packet));
                    }
                    if (etl::holds_alternative<SendPacket>(_packet))
                    {
                        return send(etl::get<SendPacket>(_packet));
                    }

                    assert(false);
                }

            private:
                etl::variant<PingPacket, PongPacket, SendPacket> _packet;
            };
        }
    }
}

#endif