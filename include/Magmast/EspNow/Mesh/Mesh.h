#ifndef MAGMAST_ESP_NOW_MESH_MESH_H
#define MAGMAST_ESP_NOW_MESH_MESH_H

#include <EspNow.h>
#include <etl/vector.h>
#include <WiFi.h>

#include "Codec.h"
#include "Node.h"
#include "Session.h"
#include "Topology.h"

#ifndef ESP_NOW_MESH_SESSION_CAPACITY
#define ESP_NOW_MESH_SESSION_CAPACITY 10
#endif

#ifndef ESP_NOW_MESH_NODES_CAPACITY
#define ESP_NOW_MESH_NODES_CAPACITY 1
#endif

#ifndef ESP_NOW_MESH_TOPOLOGY_CAPACITY
#define ESP_NOW_MESH_TOPOLOGY_CAPACITY 6
#endif

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            class Mesh
            {
            private:
                static Mesh _instance;

            public:
                static Mesh &getInstance();

                void begin();

                Node &createNode();

                Node &createNode(const EspNowAddress &&address);

                void removeNode(const Node &node);

                /// Shouldn't be used directly. Send packets using `Node` class
                /// instead.
                void send(const Packet &packet);

            private:
                etl::vector<Node, ESP_NOW_MESH_NODES_CAPACITY> _nodes;
                Session<ESP_NOW_MESH_SESSION_CAPACITY> _session;
                Codec _codec;
                std::default_random_engine _random;
                etl::optional<Topology<ESP_NOW_MESH_TOPOLOGY_CAPACITY>>
                    _topology;

                void _sendPing(const PingPacket &packet) const;
                void _sendSend(const SendPacket &packet);

                void _handleReceive(const EspNowAddress &forwarder,
                                    const etl::array_view<uint8_t> &data);
                void _handleReceivePing(const PingPacket &packet);
                void _handleReceivePong(const PongPacket &packet);
                void _handleReceiveSend(const SendPacket &packet,
                                        const etl::array_view<uint8_t> &data);

                void _forward(
                    const SendPacket &packet,
                    const etl::array_view<uint8_t> &data);
            };
        }
    }
}

#endif
