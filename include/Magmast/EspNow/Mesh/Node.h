#ifndef MAGMAST_ESP_NOW_MESH_NODE_H
#define MAGMAST_ESP_NOW_MESH_NODE_H

#include <random>

#include <EspNow.h>
#include <etl/optional.h>

#include "Packet.h"

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            class Node
            {
            public:
                using ReceiveCallback = Magmast::EspNow::EspNow::ReceiveCallback;

                Node(const EspNowAddress &&address, uint_fast32_t seed);

                const EspNowAddress &getAddress() const;
                void setAddress(const EspNowAddress &&address);

                void send(
                    const EspNowAddress &&receiver,
                    const SendPacket::Payload &&payload);

                void broadcast(const SendPacket::Payload &&payload);

                void onReceive(const ReceiveCallback &&callback);

                void removeOnReceive();

                /// Receives a packet. If the packet is sent to this node or to
                /// broadcast address, it executes `onReceive` callback and returns
                /// `true`. If packet couldn't be handled, it returns `false`.
                bool receive(const SendPacket &packet) const;

            private:
                EspNowAddress _address;
                std::default_random_engine _random;
                etl::optional<ReceiveCallback> _receiveCallback;
            };
        }
    }
}

#endif