#ifndef MAGMAST_ESP_NOW_MESH_h
#define MAGMAST_ESP_NOW_MESH_h

#include <etl/circular_buffer.h>

#include <EspNow.h>

#include "Packet.h"

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            template <size_t CAPACITY>
            class Session
            {
            private:
                struct SenderAndId
                {
                    EspNowAddress source;
                    SendPacket::Id id;
                };

            public:
                void addHandled(const SendPacket &packet)
                {
                    _handledPackets.push({
                        packet.getSender(),
                        packet.getId(),
                    });
                }

                bool isHandled(const SendPacket &packet) const
                {
                    return std::any_of(
                        _handledPackets.begin(),
                        _handledPackets.end(),
                        [&packet](const SenderAndId &senderAndId)
                        {
                            return senderAndId.id == packet.getId() &&
                                   packet.getSender() == senderAndId.source;
                        });
                }

            private:
                etl::circular_buffer<SenderAndId, CAPACITY> _handledPackets;
            };
        }
    }
}

#endif