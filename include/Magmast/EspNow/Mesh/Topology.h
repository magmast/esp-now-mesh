#ifndef MAGMAST_ESP_NOW_MESH_TOPOLOGY_H
#define MAGMAST_ESP_NOW_MESH_TOPOLOGY_H

#include <functional>

#include <EspNow.h>
#include <etl/circular_buffer.h>

#include "Codec.h"
#include "Packet.h"

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            /// Keeps track of the current node neighbours.
            template <size_t CAPACITY = 6>
            class Topology
            {
            public:
                Topology(const EspNowAddress &&address) : _address{address} {}

                /// Informs neighbour nodes to start pinging.
                void requestUpdate()
                {
                    const PingPacket packet{EspNowAddress{_address}};
                    const auto data = _codec.encode(packet);
                    ::EspNow.broadcast(data);
                }

                void respondToRequest(const PingPacket &packet)
                {
                    _update(packet.getSender());
                    const PongPacket response{EspNowAddress{_address}};
                    const auto data = _codec.encode(response);
                    ::EspNow.send(EspNowPeer{EspNowAddress{packet.getSender()}}, data);
                }

                void update(const PongPacket &packet)
                {
                    _update(packet.getSender());
                }

                etl::array_view<EspNowAddress> getNeighbours() const
                {
                    return {_neighbours.begin(), _neighbours.end()};
                }

            private:
                EspNowAddress _address;
                etl::circular_buffer<EspNowAddress, CAPACITY> _neighbours;
                Codec _codec;

                void _update(const EspNowAddress &address)
                {
                    const auto neighbourAlreadyAdded =
                        std::any_of(_neighbours.begin(), _neighbours.end(),
                                    [&](const EspNowAddress &neighbour)
                                    { return neighbour == address; });
                    if (!neighbourAlreadyAdded)
                    {
                        _neighbours.push(EspNowAddress{address});
                    }
                }
            };
        }
    }
}

#endif