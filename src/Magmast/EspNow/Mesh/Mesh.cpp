#include "Magmast/EspNow/Mesh/Mesh.h"

Magmast::EspNow::Mesh::Mesh Magmast::EspNow::Mesh::Mesh::_instance;

Magmast::EspNow::Mesh::Mesh &Magmast::EspNow::Mesh::Mesh::getInstance()
{
    return _instance;
}

void Magmast::EspNow::Mesh::Mesh::begin()
{
    ::EspNow.begin();

    EspNowAddress address;
    WiFi.macAddress(address.begin());
    _topology = Topology<ESP_NOW_MESH_TOPOLOGY_CAPACITY>{std::move(address)};
    _topology->requestUpdate();

    ::EspNow.onReceive(std::bind(
        &Mesh::_handleReceive,
        this,
        std::placeholders::_1,
        std::placeholders::_2));
}

Magmast::EspNow::Mesh::Node &Magmast::EspNow::Mesh::Mesh::createNode()
{
    EspNowAddress address;
    WiFi.macAddress(address.begin());
    return createNode(std::move(address));
}

Magmast::EspNow::Mesh::Node &
Magmast::EspNow::Mesh::Mesh::createNode(const EspNowAddress &&address)
{
    _nodes.push_back(Node{std::move(address), _random()});
    return _nodes[_nodes.size() - 1];
}

void Magmast::EspNow::Mesh::Mesh::removeNode(const Node &node)
{
    const auto iterator = std::find_if(
        _nodes.begin(),
        _nodes.end(),
        [&node](const Node &n)
        { return n.getAddress() == node.getAddress(); });

    _nodes.erase(iterator);
}

void Magmast::EspNow::Mesh::Mesh::send(const Packet &packet)
{
    /// TODO: Pings and pongs are handled by Topology class directly and that is
    ///  bad! We should change that.
    packet.map<void>(
        [=](const PingPacket &packet) {},
        [=](const PongPacket &packet) {},
        [=](const SendPacket &packet)
        {
            _sendSend(packet);
        });
}

void Magmast::EspNow::Mesh::Mesh::_sendPing(const PingPacket &packet) const
{
    ::EspNow.broadcast(_codec.encode(packet));
}

void Magmast::EspNow::Mesh::Mesh::_sendSend(const SendPacket &packet)
{
    _session.addHandled(packet);
    const auto data = _codec.encode(packet);
    _forward(packet, data);
}

void Magmast::EspNow::Mesh::Mesh::_handleReceive(
    const EspNowAddress &forwarder,
    const etl::array_view<uint8_t> &data)
{
    const auto packet = _codec.decode(data);

    packet.map<void>(
        [=](const PingPacket &packet)
        {
            _handleReceivePing(packet);
        },
        [=](const PongPacket &packet)
        {
            _handleReceivePong(packet);
        },
        [=](const SendPacket &packet)
        {
            _handleReceiveSend(packet, data);
        });
}

void Magmast::EspNow::Mesh::Mesh::_handleReceivePing(const PingPacket &packet)
{
    _topology->respondToRequest(packet);
}

void Magmast::EspNow::Mesh::Mesh::_handleReceivePong(const PongPacket &packet)
{
    _topology->update(packet);
}

void Magmast::EspNow::Mesh::Mesh::_handleReceiveSend(
    const SendPacket &packet,
    const etl::array_view<uint8_t> &data)
{
    if (_session.isHandled(packet))
    {
        return;
    }

    _session.addHandled(packet);
    _forward(packet, data);
}

void Magmast::EspNow::Mesh::Mesh::_forward(
    const SendPacket &packet,
    const etl::array_view<uint8_t> &data)
{
    bool handled = false;
    for (const auto &node : _nodes)
    {
        if (node.receive(packet))
        {
            handled = true;
        }
    }

    if (!handled || packet.getReceiver() == EspNowAddress::BROADCAST)
    {
        for (const auto neighbour : _topology->getNeighbours())
        {
            ::EspNow.send(EspNowPeer{std::move(neighbour)}, data);
        }
    }
}