#include "Magmast/EspNow/Mesh/Node.h"

#include "Magmast/EspNow/Mesh/Mesh.h"

Magmast::EspNow::Mesh::Node::Node(
    const EspNowAddress &&address,
    const uint_fast32_t seed)
    : _address{std::move(address)}, _random{seed} {}

const EspNowAddress &Magmast::EspNow::Mesh::Node::getAddress() const
{
    return _address;
}

void Magmast::EspNow::Mesh::Node::setAddress(const EspNowAddress &&address)
{
    _address = std::move(address);
}

void Magmast::EspNow::Mesh::Node::send(
    const EspNowAddress &&receiver,
    const SendPacket::Payload &&payload)
{
    const SendPacket sendPacket{
        _random(),
        EspNowAddress{_address},
        std::move(receiver),
        std::move(payload)};

    const Packet packet{std::move(sendPacket)};

    Mesh::getInstance().send(packet);
}

void Magmast::EspNow::Mesh::Node::broadcast(const SendPacket::Payload &&payload)
{
    send(
        EspNowAddress{EspNowAddress::BROADCAST},
        std::move(payload));
}

void Magmast::EspNow::Mesh::Node::onReceive(const ReceiveCallback &&callback)
{
    _receiveCallback = std::move(callback);
}

void Magmast::EspNow::Mesh::Node::removeOnReceive()
{
    _receiveCallback = etl::nullopt;
}

bool Magmast::EspNow::Mesh::Node::receive(const SendPacket &packet) const
{
    if (!packet.isSentBy(_address) &&
        (packet.isSentTo(_address) ||
         packet.isSentTo(EspNowAddress::BROADCAST)) &&
        _receiveCallback)
    {
        (*_receiveCallback)(
            packet.getSender(),
            packet.getPayload());
        return true;
    }

    return false;
}