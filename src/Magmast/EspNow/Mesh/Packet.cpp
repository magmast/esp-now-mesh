#include "Magmast/EspNow/Mesh/Packet.h"

Magmast::EspNow::Mesh::PingPacket::PingPacket(const Address &&sender)
    : _sender{std::move(sender)} {}

const Magmast::EspNow::Mesh::PingPacket::Address &
Magmast::EspNow::Mesh::PingPacket::getSender() const
{
    return _sender;
}

bool Magmast::EspNow::Mesh::PingPacket::isPing() const
{
    return true;
}

Magmast::EspNow::Mesh::PongPacket::PongPacket(const Address &&sender)
    : _sender{std::move(sender)} {}

const Magmast::EspNow::Mesh::PongPacket::Address &
Magmast::EspNow::Mesh::PongPacket::getSender() const
{
    return _sender;
}

bool Magmast::EspNow::Mesh::PongPacket::isPong() const
{
    return true;
}

Magmast::EspNow::Mesh::SendPacket::SendPacket(
    const Id id,
    const Address &&sender,
    const Address &&receiver,
    const Payload &&payload)
    : _id{id}, _sender{std::move(sender)}, _receiver{std::move(receiver)},
      _payload{std::move(payload)}
{
}

Magmast::EspNow::Mesh::SendPacket::Id
Magmast::EspNow::Mesh::SendPacket::getId() const
{
    return _id;
}

const Magmast::EspNow::Mesh::SendPacket::Address &
Magmast::EspNow::Mesh::SendPacket::getSender() const
{
    return _sender;
}

bool Magmast::EspNow::Mesh::SendPacket::isSentBy(const Address &other) const
{
    return _sender == other;
}

const Magmast::EspNow::Mesh::SendPacket::Address &
Magmast::EspNow::Mesh::SendPacket::getReceiver() const
{
    return _receiver;
}

bool Magmast::EspNow::Mesh::SendPacket::isSentTo(const Address &other) const
{
    return _receiver == other;
}

bool Magmast::EspNow::Mesh::SendPacket::isSentTo(const EspNowPeer &peer) const
{
    return isSentTo(peer.getAddress());
}

const Magmast::EspNow::Mesh::SendPacket::Payload &
Magmast::EspNow::Mesh::SendPacket::getPayload() const
{
    return _payload;
}

bool Magmast::EspNow::Mesh::SendPacket::isSend() const
{
    return true;
}

Magmast::EspNow::Mesh::Packet
Magmast::EspNow::Mesh::Packet::ping(const Address &&sender)
{
    return Packet{PingPacket{std::move(sender)}};
}

Magmast::EspNow::Mesh::Packet
Magmast::EspNow::Mesh::Packet::pong(const Address &&sender)
{
    return Packet{PongPacket{std::move(sender)}};
}

Magmast::EspNow::Mesh::Packet Magmast::EspNow::Mesh::Packet::send(
    SendPacket::Id id,
    const EspNowAddress &&sender,
    const EspNowAddress &&receiver,
    const etl::array_view<uint8_t> &&payload)
{
    return Packet{SendPacket{
        id,
        std::move(sender),
        std::move(receiver),
        std::move(payload)}};
}

Magmast::EspNow::Mesh::Packet::Packet(const PingPacket &&packet)
    : _packet{packet} {}

Magmast::EspNow::Mesh::Packet::Packet(const PongPacket &&packet)
    : _packet{packet} {}

Magmast::EspNow::Mesh::Packet::Packet(const SendPacket &&packet)
    : _packet{packet} {}

const Magmast::EspNow::Mesh::Packet::Address &
Magmast::EspNow::Mesh::Packet::getSender() const
{
    return map<const Address &>(
        [](const PingPacket &ping)
        { return ping.getSender(); },
        [](const PongPacket &pong)
        { return pong.getSender(); },
        [](const SendPacket &send)
        { return send.getSender(); });
}

bool Magmast::EspNow::Mesh::Packet::isPing() const
{
    return etl::holds_alternative<PingPacket>(_packet);
}

bool Magmast::EspNow::Mesh::Packet::isPong() const
{
    return etl::holds_alternative<PongPacket>(_packet);
}

bool Magmast::EspNow::Mesh::Packet::isSend() const
{
    return etl::holds_alternative<SendPacket>(_packet);
}