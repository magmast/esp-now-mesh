#include "Magmast/EspNow/Mesh/Codec.h"

#include <etl/byte_stream.h>

constexpr const uint8_t PING_TYPE = 0;
constexpr const uint8_t PONG_TYPE = 1;
constexpr const uint8_t SEND_TYPE = 2;

namespace Magmast::EspNow::Mesh
{
    etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
    Encoder::encode(const Packet &packet) const
    {
        return packet.map<const etl::vector<
            uint8_t,
            IPacket::MAX_SIZE_IN_BYTES>>(
            [=](const PingPacket &packet)
            { return encode(packet); },
            [=](const PongPacket &packet)
            { return encode(packet); },
            [=](const SendPacket &packet)
            { return encode(packet); });
    }

    etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
    Encoder::encode(const PingPacket &packet) const
    {
        etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES> data;

        data.push_back(PING_TYPE);

        const auto sender = packet.getSender();
        for (auto byte = sender.cbegin(); byte != sender.cend(); ++byte)
        {
            data.push_back(*byte);
        }

        return data;
    }

    etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
    Encoder::encode(const PongPacket &packet) const
    {
        etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES> data;

        data.push_back(PONG_TYPE);

        const auto sender = packet.getSender();
        for (auto byte = sender.cbegin(); byte != sender.cend(); ++byte)
        {
            data.push_back(*byte);
        }

        return data;
    }

    etl::vector<uint8_t, IPacket::MAX_SIZE_IN_BYTES>
    Encoder::encode(const SendPacket &packet) const
    {
        etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES> data;

        data.push_back(SEND_TYPE);

        const auto id = packet.getId();
        const auto idBytes = reinterpret_cast<const uint8_t *>(&id);
        for (auto byte = idBytes; byte != idBytes + sizeof(id); ++byte)
        {
            data.push_back(*byte);
        }

        const auto sender = packet.getSender();
        for (auto byte = sender.cbegin(); byte != sender.cend(); ++byte)
        {
            data.push_back(*byte);
        }

        const auto receiver = packet.getReceiver();
        for (auto byte = receiver.cbegin(); byte != receiver.cend(); ++byte)
        {
            data.push_back(*byte);
        }

        const auto payload = packet.getPayload();
        for (auto byte = payload.cbegin(); byte != payload.cend(); ++byte)
        {
            data.push_back(*byte);
        }

        return data;
    }

    Packet Decoder::decode(
        const etl::array_view<uint8_t> &data) const
    {
        if (data[0] == PING_TYPE)
        {
            return decodePing(data);
        }
        if (data[0] == PONG_TYPE)
        {
            return decodePong(data);
        }
        if (data[0] == SEND_TYPE)
        {
            return decodeSend(data);
        }

        abort();
    }

    Packet Decoder::decodePing(
        const etl::array_view<uint8_t> &data) const
    {
        PingPacket::Address sender{data.begin() + 1};
        return Packet::ping(std::move(sender));
    }

    Packet Decoder::decodePong(
        const etl::array_view<uint8_t> &data) const
    {
        PongPacket::Address sender{data.begin() + 1};
        return Packet::pong(std::move(sender));
    }

    Packet Decoder::decodeSend(
        const etl::array_view<uint8_t> &data) const
    {
        SendPacket::Id id = *reinterpret_cast<const SendPacket::Id *>(
            data.data() + 1);

        SendPacket::Address sender{data.begin() + 1 + sizeof(SendPacket::Id)};

        SendPacket::Address receiver{data.begin() + 1 + sizeof(SendPacket::Id) +
                                     EspNowAddress::SIZE};

        SendPacket::Payload payload{
            data.begin() + 1 + sizeof(SendPacket::Id) + EspNowAddress::SIZE * 2,
            data.end()};

        return Packet::send(
            id,
            std::move(sender),
            std::move(receiver),
            std::move(payload));
    }

    etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
    Codec::encode(const Packet &packet) const
    {
        return encoder.encode(packet);
    }

    etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
    Codec::encode(const PingPacket &packet) const
    {
        return encoder.encode(packet);
    }

    etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
    Codec::encode(const PongPacket &packet) const
    {
        return encoder.encode(packet);
    }

    etl::vector<uint8_t, Packet::MAX_SIZE_IN_BYTES>
    Codec::encode(const SendPacket &packet) const
    {
        return encoder.encode(packet);
    }

    Packet Codec::decode(
        const etl::array_view<uint8_t> &packet) const
    {
        return decoder.decode(packet);
    }
}
