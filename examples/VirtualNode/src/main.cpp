#include <EspNowMesh.h>

class LightNode
{
public:
    void begin()
    {
        pinMode(LED_BUILTIN, OUTPUT);
        digitalWrite(LED_BUILTIN, LOW);
    }

    void onReceive(const EspNowAddress &sender, const etl::array_view<uint8_t> &payload)
    {
        if (payload[0] == 0)
        {
            digitalWrite(LED_BUILTIN, state ? LOW : HIGH);
            state = !state;
        }
    }

private:
    bool state = false;
};

LightNode lightNode;
EspNowMeshVirtual<ESP_NOW_MESH_SESSION_CAPACITY> *buttonMesh;

void setup()
{
    EspNowMesh.begin();

    lightNode.begin();
    EspNowMesh.onReceive(std::bind(&LightNode::onReceive, &lightNode, std::placeholders::_1, std::placeholders::_2));

    buttonMesh = &EspNowMesh.createVirtualNode(EspNowAddress({0x01, 0x23, 0x45, 0x67, 0x89, 0xAB}));
}

bool prev;

void loop()
{
    const auto curr = digitalRead(D8) == HIGH;
    if (!prev && curr)
    {
        buttonMesh->broadcast({0});
    }
    prev = curr;
}